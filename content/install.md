## Install

- Windows: Install 'Git for Windows'
  - https://git-for-windows.github.io
- Mac: Type 'git' in the Terminal application.
  - If it's not installed, it will prompt you to install it.
- Linux
  - Debian: 'sudo apt-get install git-all'
  - Red Hat 'sudo yum install git-all'
