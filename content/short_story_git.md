## Short Story of Git

- 1991-2002: The Linux kernel was being maintained by sharing archived files
  and patches.
- 2002: The Linux kernel project began using a DVCS called BitKeeper
- 2005: BitKeeper revoked the free-of-charge status and Git was created
